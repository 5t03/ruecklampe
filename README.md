# README #

This README would normally document whatever steps are necessary to get your application up and running.


=======
### Websitecode of rleuchte.de ###

* This is the nanoc static site generator code repo for rleuchte.de
* Version 0.1.0

### How do I get set up? ###


* Get the code:
** install git (http://git-scm.com/downloads)
** type "git clone git@bitbucket.org:5t03/ruecklampe.git" or "git clone https://bitbucket.org/5t03/ruecklampe.git".

* Install Nanoc (http://nanoc.ws/install/):
** first you will need ruby (https://www.ruby-lang.org/)
** then you will need to install ruby gems (http://rubygems.org/pages/download)
** then you can call "gem install nanoc"

* Building is quite easy. Just type "nanoc" or "nanoc compile" (you have to be in the ruecklampe directory)

* to get a feeling how the site looks like you can gen nanoc to view the site by "nanoc view" then browse to "localhost:3000"

### Who do I talk to? ###

* 5t03 in Bitbucket.org
* ruecklampe.de
=======
* You need the code repo:
** downloaded from here (the cloud sign with the arrow pointing down on the right)
** or by git
*** install git (http://git-scm.com/downloads)
*** get the code by typing "git clone https://bitbucket.org/5t03/ruecklampe.git"

* Compile and View with nanoc (http://nanoc.ws/install/)
# install Ruby (https://www.ruby-lang.org/)
# install ruby gems (http://rubygems.org/pages/download)
# compile website:
## browse to ruecklampe folder (in your console program)
## type "nanoc" (this compiles the code)
## type "nanoc view" (this runs a local webserver with the compiled site)
## browse to http://localhost:3000/ (with your webbrowser)

### Contribution guidelines ###

* No guidelines yet

### Who do I talk to? ###

* 5t03 at Bitbucket
* the dudes and sheilas from zuruckutzer.de