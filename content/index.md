---
title: Home
---
  
reparierbar
: Jedes Teil als Ersatzteil erhältlich.

---

ressourcenschonend
: Die Befreiung vom Wegwerfzwang.

---

regional
: Unabhängigkeit von zentraler Massenproduktion.

---

evolutionär
: Ein Produkt mit geschlossenem Lebenszyklus.