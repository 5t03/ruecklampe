---
title: Konzept
nav_title: konzept
---
  
####Unser Konzept in aller Kürze

Wir sehen heute eine Vielzahl von sozialen und Umweltproblemen.
Diese werden durch wenig durchdachte oder sogar unsinnige Produkte
hervorgerufen. 
Nun könnte man sich auf den Standpunkt zurückziehen, das Problem sei nicht technischer Natur und statt zu handeln auf gesetzliche Rahmendbedingungen warten, die irgendwann "ganz natürlich" zu anderen Produkten führen werden.

Ja, kann sein.

Da es diese Rahmenbedingungen aber noch nicht gibt, sich
diese genaugenommen in weiten Teilen noch nichteinmal abzeichnen,
hat uns interessiert, wie ein Produkt gestaltet werden muss,
damit während seiner Herstellung, seinem Gebrauch und
auch danach, so wenig wie möglich Probleme verursacht werden.
Sozusagen als Vorbereitung für eine Zeit, in der die
Rahmenbedingungen dann bestehen werden und alle Produkte
dann "ganz natürlich" so oder ähnlich aufgebaut sein werden.

Unter Problemen verstehen wir vorrangig

1. die Ausbeutung der Umwelt und von Menschen bei der Ressourcenextraktion und der Herstellung,

2. den Energieverbrauch während dem Betrieb, sofern das Produkt mit einer Energieform betrieben wird, und

3. die Ausbeutung der Umwelt und von Menschen zur Entsorgung oder bei der Verwertung.

Derzeit gängig ist bei Elektronikprodukten die
fast exklusive Problemeingrenzung auf Punkt 2,
wobei die Punkte 1 und 3 zunehmend ins Blickfeld
eines breiteren Publikums gelangen.

Unser Konzept zur Produktentwicklung versucht allen drei
Punkten Rechnung zu tragen, um am Ende ein maximal
rücksichtsvolles Produkt zu erhalten.

[Unser Konzept im Vergleich zu derzeit gängigen](/conceptCompare/)