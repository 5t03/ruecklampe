---
title: Unser Konzept im Vergleich
nav_title: konzept
---
    
###Unser Konzept im Vergleich zu derzeit gängigen

Die technische Entwicklung eines Produkts findet in einem
produzierenden Gewerbe idealerweise nach der Erstellung der
Produktstrategie und nicht nach der Produktvermarktung statt,
zumindest jedoch, und dies allerdings zwingend, vor der
Auslieferung des Produkts.

Bei der Entwicklung eines elektronischen Produkts
ist eine bestimmte Reihenfolge von Arbeitsschritten
zu beachten, damit am Ende nicht nur ein funktionierendes,
sondern auch ein Produkt vorliegt, das beispielsweise
bestimmten Qualitätskriterien genügt.

Ein gängiges Entwicklungskonzept könnte man in diese
Arbeitsschritte einteilen (anzumerken ist noch, dass
die wichtigsten Punkte, also die höchsten Prioritäten,
auch die höchsten Werte haben und in der Liste unten
stehen):

1. Funktion (technisches Design, Konstruktion)
2. Design (Ästhetik, Corporate Design)
3. Kostenoptimierung Bauteile und Materialien
4. Kostenoptimierung Herstellung
5. Lebensdauer"optimierung" (geplante Obsoleszenz?)
6. optimale Betriebssicherheit
7. Normkonformität
8. Gesetzliche Anforderungen


Diese oder eine ähnliche Liste wird von einem
Produktentwickler theoretisch genau einmal durchlaufen,
um am Ende das fertige Produkt zu erhalten.
In der Praxis ergeben sich aber häufig
mehrere (teilweise) Durchläufe der Arbeitsschritte.
Falls beispielsweise bei Tests der Normkonformität
festgestellt wird, dass es eine Konstruktionsschwäche
gibt, dann kann es vorkommen, dass alle vorherigen Punkte
nochmals bearbeitet werden müssen, sprich: das Design
muss angepasst werden, andere Bauteile müssen eingesetzt
werden, die Herstellungsprozesse ändern sich dadurch, etc.
Deshalb ist es unter anderem wirtschaftlich, die Liste an
Arbeitsschritten so kurz wie möglich zu halten, da je länger
die Liste, um so teuerer der Prozess, und damit das Produkt.

Unser Entwicklungskonzept unterscheidet sich
nun in mehreren Punkten vom genannten gängigen Konzept.
Zum einen liegt unser Fokus nicht auf einer rein
wirtschaftlichen Optimierung des Entwicklungsprozesses,
da die gängigen Optimierungsstrategien viele Faktoren,
wie zum Beispiel soziale Faktoren oder solche der
Umweltfreundlichkeit oder Ressourcenschonung,
schlicht vernachlässigt. In einer vollständigen
Kostenbetrachtung müssen diese Faktoren auftauchen,
jedoch gibt es derzeit meist keine anerkannte
Methoden zur genauen Folgekostenbestimmung von,
beispielsweise, unfairen Arbeitsbedingungen.
In einer vollständigen Betrachtung müssten die
sozialen Folgekosten von unterbezahlten Arbeitskräften
sich im Produktpreis wiederfinden, beispielsweise
über eine höhere Zölle oder Steuern. Dies ist
in der Praxis aber nicht der Fall und diese
Kosten werden schlicht ignoriert.

Anstatt diese Faktoren enfach zu ignorieren und weil
es keine standardisierten, quantitativen Bewertungskriterien
dafür gibt, setzen wir deshalb eine maximale
Vermeidungsstrategie an.

Der erste Schritt hierbei besteht darin,
die Probleme beim Namen zu nennen und an
geeigneter Stelle im Entwicklungsablauf zu
berücksichtigen. Das hat uns zur folgenden,
angepassten Liste geführt:

1. <span style="text-decoration: line-through;">Funktion</span>
1. __Design__
2. <span style="text-decoration: line-through;">Desig</span>
2. __Funktion__
3. <span style="text-decoration: line-through;">Kostenoptimierung Bauteile und Materialien</span>
3. __Faire Lieferkette und umweltfreundliche Materialien__
4. <span style="text-decoration: line-through;">Kostenoptimierung Herstellung</span>
4. __Minimaler Materialaufwand__
5. <span style="text-decoration: line-through;">Lebensdauer"optimierung" (geplante Obsoleszenz?)</span>
5. __Lebensdaueroptimierung im Sinne der Lebensdauermaximierung__
6. <span style="text-decoration: line-through;">optimale Zuverlässigkeit</span>
6. __konservative Zuverlässigkeit__
7. __Reparierbarkeit__
8. Normkonformität
9. Gesetzliche Anforderungen

Zunächst werden ästhetische Eigenschaften den funktionalen
nachgeordnet, also: design by function. Dies ist aus unserer
Sicht zwingend notwendig, da die Funktion selbst durch nachfolgende
Punkte beeinflußt wird, wie beispielsweise die Auswahl von
schadstofffreien Materialen. Schon an diesem Punkt zeigt sich,
dass dieser Entwicklungsprozess "teurer" im Sinne bestehender
Kostenmetriken ausfallen wird, da sich am Ergebnis der
Funktionserstellung formal nichts ändert, dieser Arbeitsschritt
jedoch deutlich langwieriger weil aufwändiger ausfällt, da vor
der Umsetzung mehr Faktoren eingeplant werden müssen.
Wo in einem normalen Entwicklungsprozess der Arbeitsschritt
also abgeschlossen werden könnte, wird er im vorliegenden
Entwicklungsprozess zunächst wiederholt revidiert und erneut
durchlaufen (iterativ), bis alle Anforderungen maximal erfüllt
sind.

An dieser Stelle muss nochmals darauf hingewiesen werden,
dass bestehende Kostenmetriken aufgrund weitreichender
Vernachlässgungen von tatsächlicen sozialen Kosten sowie realen
Umweltschäden keine vollständige Berücksichtigung der Realität
zugrundelegen und deshalb unvollständig sind und als theoretisch
bezeichnet werden müssen. Diese theoretischen Kosten werden
dann aber von dem bestehenden Wirtschaftsmodell in der Praxis
bestätigt und die Differenz zu den realen Kosten ungefragt
und indirekt heute oder in Zukunft lebenden Menschen aufgezwungen.

Die reine Kostenoptimierung beim Bauteil- und Materialeinkauf
wird in unserem Fall ersetzt durch die Berücksichtigung von
sozialen Parametern und einer Priorisierung von
umweltfreundlichen Materialien.

Die Kostenoptimierung bei der Herstellung wird ersetzt durch
den bewussten Einsatz von so wenig Material wie möglich.
Dies spart Ressourcen bei der Herstellung und begrenzt etwaige
Schäden bei der unsachgemäßen Entsorgung (also dem Verzicht auf
Reparatur) auf ein Minimum.

Die Lebensdaueroptimierung, die oft maßgeblich über das
Geschäftsmodell des Herstellers bestimmt wird, wird bei uns
auf maximale Lebensdauer ausgelegt. Dies kann sogar zu
Abstrichen bei den Leistungsdaten führen, also auf die
Funktion zurückwirken, was in der Form bei einem "Verbrauchsartikel"
oder einem Wegwerfprodukt nicht gängig ist.

Bei der Auslegung der Zuverlässigkeit macht es Sinn, oft auch im
Zusammenspiel mit einer Lebensdauermaximierung, das Produkt nicht
am Limit auszulegen. Konkret werden Sicherheitsabstände grosszügig
ausgelegt, um auch bei Verwendung der Produkte in seltenen
Bedingungen eine deutlich längere Lebensdauer und Zuverlässigkeit
zu erzielen.

Die vorgesehene Reparierbarkeit ist wesentliches Abgrenzungskriterium
zum Verbrauchsartikel oder Wegwerfprodukt und deshalb in unserer
Liste die höchste, selbstgewählte Priorität.

Nur die Industriestandards (Normen) und die gesetzlichen Anforderungen,
die oft auch fliessend ineinander übergehen, haben eine höhere Priorität
als die Reparierbarkeit.


###Konkretes Beispiel Kunstoffteil:

Normalerweise wird ein Kunsstoffteil durch seine Form
und diverse technische Eigenschaften "definiert", worauf
die Auswahl eines Herstellers oder Lieferanten getroffen wird:

1. weisse Farbe
2. definierte elektrische Isolationsfestigkeit
3. Hitze- und Kältebeständigkeit
  (z.B. schmilz nicht bei Hitze und bricht nicht bei Kälte)
4. leichte Ver- oder Bearbeitbarkeit mit gängigen Maschinen
5. minimaler Preis

Ein solches Kunststoffteil ist real erhältlich.
Daß dieses Teil irgendwann in einem Entwicklungsland von Kindern
verbrannt und eingeatmet wird oder von Meeresbewohnern verschluckt
wird und nicht verdaut werden kann spielt im gängigen Entwicklungsprozess
keine Rolle, die Praxis zeigt jedoch, dass dies weder ein
theoretischer, noch ein unwahrscheinlicher Teil des Lebenszyklus
eines solchen Kunststoffteils ist.

Die teilweise gegensätzlichen Anforderungen aus unseren
Prioritäten an das Material soll nun anhand  der Auswahl
eines hypothetischen Kunststoffteils verdeutlicht werden.

Folgende Eigenschaften wären wünschenswert (Beispiel):

1. weisse Farbe
2. keine Zusatzstoffe
  (insbesondere keine Schadstoffbildung beim Verbrennen
  in kleinen Lagerfeuern)
3. vollständige Kompostierbarkeit (oder sogar: Nahrhaftigkeit für Fische!)
4. nicht aus Erdöl hergestellt, falls doch: recyceltes Material
5. faire Herstellung
6. lokale Herstellung
7. definierte elektrische Isolationsfestigkeit
8. Hitze- und Kältebeständigkeit
  (z.B. schmilz nicht bei Hitze und bricht nicht bei Kälte)
9. leichte Ver- oder Bearbeitbarkeit mit gängigen Maschinen
10. vergleichbarer Preis zu gängigem Kunststoff

Real gibt es einen solchen (Kunst-) Stoff nicht (Hinweise jederzeit
willkommen, falls doch).