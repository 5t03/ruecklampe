---
title: Produktinfo
nav_title: leuchte
olnav: "pro<span style=\"opacity: 0.6\">dukt</span>"
---
  
Produktionsstandards
: 
- regionale Produktion möglich (im ersten Schritt noch nicht umsetzbar)
- regionaler Materialkreislauf durch Reparatur
- Unabhängig, weil flexible und dezentrale Produktionsstruktur 
- kann Arbeitsplätze sichern & erzeugen, da die Herstellung & Reparatur manuell möglich sind 
- quecksilber- & amalgamfreies Produkt!

Lichtfarbe
: - warm-weiss (wie warmes Glühbirnenlicht)

Lichttemperatur
: - 2700 K Farbwiedergabeindex Ra > 80

Lichtstärke
: - {::nomarkdown}>{:/}200 lm, Öffnungswinkel Lichtkegel: ca. 130°

Leistungsaufnahme
: - 2.8 W

Lichtausbeute und Energieeffizienzklasse
: - {::nomarkdown}>{:/}200 lm / 2.8 W  = > 71 lm / W (Lichtstärke / el. Leistungsaufnahme)      
-> Energieffizienzklasse A+

Lebensdauer
: 
- ein früher Prototyp leuchtet durchgehend seit Dezember 2012
- _der_ Lebensdauer-Parameter bei LED-Lampen ist die Betriebstemperatur
-> Kühlung

Preisvorteile
: 
- Spart Energie im Betrieb
- Lange Lebensdauer spart Zeit, Rohstoffe, Energie & Kosten bei der
Herstellung von Austauschleuchtmitteln
- Wiederverwendbarkeit von nicht-verschleißbehafteten Bauteilen
spart Zeit, Rohstoffe, Energie & Kosten einer kompletten Neuproduktion

Energieverschwendung
: 
- Produktionsenissionen werden durch einen schlanken Herstellungsprozess vermieden
- Bedarfsnahe Herstellung erlaubt transportbedingte Energieeinsparungen
- Energiesparender Betrieb durch LED-Technik
- Wiederverwendung von nicht-verschleißbehafteten Teilen nach Ende der
Lebensdauer der verschleißenden LED's sparen Energie, da sie nicht erneut
hergestellt und transportiert werden müssen.
    
  
