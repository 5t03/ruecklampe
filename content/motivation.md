---
title: Motiv
nav_title: "motiv<span style=\"opacity: 0.6\">ation</span>"
---

Wir sind von der [Obsoleszenz] der [Obsoleszenz] überzeugt.
[Obsoleszenz]: https://de.wikipedia.org/wiki/Obsoleszenz

Damit ist gemeint, dass die [Wegwerfgesellschaft](https://de.wikipedia.org/wiki/Wegwerfgesellschaft) nicht mehr modern ist,
da sie im Widerspruch zu natürlichen Kreislaufprozessen steht.

Diese These wird dadurch begründet, dass der Wegwerfzwang über
die gängigen Produktdesigns dem Käufer aufgezwungen wird.
Wir glauben, dass unsere Konsumgesellschaft sich an diesem Punkt
sehr leicht weiterentwickeln kann.
[Konsumentensouveränität](https://de.wikipedia.org/wiki/Konsumentensouver%C3%A4nit%C3%A4t)

Die durch den Wegwerfzwang verursachte, vorprogrammierte Abwertung
der Produkte zu Müll stellt einen wichtiger Aspekt der
Wertschöpfungskette dar, deren Kehrseite jedoch die Auslagerung
des abgeschöpften Wertes in soziale und Umweltkosten ist.
[Externer Effekt](https://de.wikipedia.org/wiki/Externer_Effekt)

Konkret ist der Umgang mit dem Müll nach der Nutzung des
Produkts alles andere als umweltfreundlich, insbesondere
im Fall von dem sogenannten [Elektronikschrott](https://de.wikipedia.org/wiki/Elektronikschrott).

Diese Kosten sind im Preis des Produkts nicht enthalten, sondern
werden teilweise von der Allgemeinheit, jedoch zum größten Teil
von zukünftigen Generationen getragen (z.B. [Plastikmüll in den Ozeanen](https://de.wikipedia.org/wiki/Plastikm%C3%BCll_in_den_Ozeanen)).

Darüberhinaus ist auch der soziale Mehrwert von vielen
Produkten durch die systematische Ausbeutung von Arbeitskräften,
insbesondere in den sogenannten Billiglohnländern, oft ein
negativer Wert und damit kein Mehrwert.
Das heisst diese Menschen (produktiven Arbeitskräfte) bezahlen durch
den Verzicht auf Lebensstandard, Rechte oder sogar ihre Gesundheit
einen unfreiwilligen Anteil an den Gesamtkosten des Produkts.

Auf dieser Basis können also viele Produkte und ihr Design als
umweltschädlich und unsozial erkannt werden, anstatt wie üblich
nur den Energieverbrauch der Produkte während der Nutzung durch
den sogenannten Verbraucher als Maß für die Umweltfreundlichkeit
heranzuziehen.
[Energieeinsparung](https://de.wikipedia.org/wiki/Energieeinsparung)

Hier setzen wir an und optimieren das Produktdesign
auf den gesamten Lebenszyklus hin und maximieren dadurch
die Umweltfreundlichkeit und den sozialen Mehrwert des Produkts.
[Corporate Social Responsibility](https://de.wikipedia.org/wiki/Corporate_Social_Responsibility)


Externe Links:
---

 [The Story of Stuff](http://www.storyofstuff.org/)

 [The Story of Electronics](http://storyofstuff.org/movies/story-of-electronics/)

 [Forum InformatikerInnen für Frieden und gesellschaftliche Verantwortung e.V.](http://www.fiff.de)

 [Faire Computermaus von NagerIT](http://www.nager-it.de)

 [Faires Mobiltelefon](http://www.fairphone.com/)